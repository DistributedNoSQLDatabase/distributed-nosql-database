This is the private Git repository for the EECE411 Project on Distributed Systems
https://bitbucket.org/DistributedNoSQLDatabase/distributed-nosql-database

PL Slice = ubc_eece411_4
Group # 8

Name: Jose Pinilla
StudentID: 78737146

Name: Abdulaziz Alghamdi
StudentID: 46264073

Service Port: 5600

Design Status: Multiple-node routed system

1. Design Choices:
The design is packaged in one JAR using Apache Maven for Eclipse M2E. The package contains 3 main Classes that instantiate the underlying functionality of the KVStore system.

KVClientTest: Runs a request by means of command line arguments. This class is used for tests.
   Usage Syntax
      >>java KVClientTest -n <hostname> -p <port> -s <value> <key> [-t <Retry timeout>]
      >>java KVClientTest -n <hostname> -p <port> [-g] <key>
      >>java KVClientTest -n <hostname> -p <port> -r <key>
      >>java KVClientTest -n <hostname> -k <key>
      >>java KVClientTest -n <hostname> -S <valuefile> <key>
		  >>java KVClientTest -n <hostname> -T <kvpairfile>

KVServerRun: Server class attends KVStore requests and stores in a HashTable. 
   Usage Syntax
      >>java KVServerRun -p <port>

KVManager: Also a client but its interface resembles Redis (An advanced key-value cache and store) on a simple prompt for quick user interaction. KVManager prints out "KVManager>" and expects user inputs in the following formats. Useful for server interaction
   Usage Syntax
      >>java KVManager -p <port> -n <hostname>

   Prompt usage
		>>KVManager> SET <key>|<value>
		>>KVManager> GET <key>
		>>KVManager> REMOVE <key>
		>>KVManager> KILL <key>

2. Tests description

STEP 4 Tests:

Using the monitoring system from STEP 2 a set of nodes between 50 and 200 is chosen to perform tests for distributed keys routing. (A set of 50 nodes is sumbitted as specified by the requirements)
The data on the KVData.csv and KVLargeData.csv files were generated using www.generatedata.com in order to have a balanced load test of the system but at the same time representing a real world application.
	KVData: <Name Lastname>|<E-mail>
	KVLargeData: <Postal Code>|<Strings ~1000 characters>
These files are used to SET all keys and measure SET time using the -T switch of the KVClientTest. The results are attached in the "logTime.log" file and the spreadsheet "Results.ods"
Number of requests: 400
Average response time: 68.78ms
Number of failed requests: 11 (2.75%)
Number of requests that needed to retry: 7 (1.75%)

Lists of nodes where the program has been deployed. These lists where generated taking into account which were available at the time of submision of the assignment and have the latest version of java installed.

6 lists of 3 nodes:
./KVStore/test/assi4Nodes/assi4Nodes_1.txt
./KVStore/test/assi4Nodes/assi4Nodes_2.txt
./KVStore/test/assi4Nodes/assi4Nodes_3.txt
./KVStore/test/assi4Nodes/assi4Nodes_4.txt
./KVStore/test/assi4Nodes/assi4Nodes_5.txt
./KVStore/test/assi4Nodes/assi4Nodes_6.txt

1 list of 50 nodes
./KVStore/test/assi4Nodes/assi4Nodes_50.txt


(NOTE: ./KVStore/test/assi4Nodes/assi4Nodes_111.txt was used for internal testing, we do not ensure to have all of this list of nodes working, but we often deply to this list)

Additional files:

test.sh <CASE>: this file allows multiple different tests for setting, getting, removing using the switch cases and data files

deploy.sh: this file allows the deployment of the service and its execution once copied to the corresponding nodes


STEP 3 Tests:
In order to test, 3 locations have been chosen to independtly deploy the service. A list of nodes assi3Nodes.txt is packaged with the project files listing the best 10 response times (PING):
   Others nodes are in the range of 100-250ms:
		- planetlab2.cs.ubc						0.00 ms
		- cs-planetlab3.cs.surrey.sfu			6.27 ms
		- planetlab4.cs.uoregon					12.1 ms
		- planetlab1.unr						29.2 ms
		- planetlab2.unr					    30.4 ms
		- planetlab1.cs.ucla					32.8 ms
		- planetlab1.eecs.wsu					33.8 ms
		- planetlab1.unl						47.3 ms
		- planetlab-1.cs.uic					48.0 ms
		- planetlab2.cs.purdue					62.7 ms

Using Parallel SCP we copied the JAR package for the project to each of these nodes and tested them using KVManager on localhost for multiple GET,SET,REMOVE,KILL operations. Once deployed the nodes can be tested using either KVManager or KVClientTest from any compatible host. The test.sh script runs the available operations and returns the execution time. Times obtained are presented on the following section.

3. Performance characterization
(If table does not display properly, please see attached image performance_graph.png)
+--------------------------------+---------+---------+------------+
|              Node              | SET(ms) | GET(ms) | REMOVE(ms) |
+--------------------------------+---------+---------+------------+
| planetlab2.cs.ubc.ca           | 0.13    | 0.14    | 0.13       |
| cs-planetlab3.cs.surrey.sfu.ca | 0.12    | 0.13    | 0.14       |
| planetlab4.cs.uoregon.edu      | 0.13    | 0.14    | 0.12       |
| planetlab1.unr.edu             | 0.12    | 0.13    | 0.13       |
| planetlab2.unr.edu             | 0.14    | 0.15    | 0.13       |
| planetlab1.cs.ucla.edu         | 0.14    | 0.14    | 0.13       |
| planetlab1.unl.edu             | 0.12    | 0.15    | 0.13       |
| planetlab2.cs.purdue.edu       | 0.12    | 0.16    | 0.13       |
+--------------------------------+---------+---------+------------+