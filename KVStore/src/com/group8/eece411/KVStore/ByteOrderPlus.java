package com.group8.eece411.KVStore;


import java.io.*;


/**
 * Various static routines for solving endian problems.
 */
public class ByteOrderPlus {
	
    /**
     * Returns the reverse of x.
     */
    public static byte[] reverse(byte[] x) {
        int n=x.length;
        byte[] ret=new byte[n];
        for (int i=0; i<n; i++) 
            ret[i]=x[n-i-1];
        return ret;
    }

    /** 
     * Little-endian bytes to int
     * 
     * @requires x.length-offset>=4
     * @effects returns the value of x[offset..offset+4] as an int, 
     *   assuming x is interpreted as a  signed little endian number (i.e., 
     *   x[offset] is LSB) If you want to interpret it as an unsigned number,
     *   call ubytes2long on the result.
     */
    public static int leb2int(byte[] x, int offset) {
        //Must mask value after left-shifting, since case from byte
        //to int copies most significant bit to the left!
        int x0=x[offset] & 0x000000FF;
        int x1=(x[offset+1]<<8) & 0x0000FF00;
        int x2=(x[offset+2]<<16) & 0x00FF0000;
        int x3=(x[offset+3]<<24);
        return x3|x2|x1|x0;
    }
    
    /**
     * Big-endian bytes to int.
     *
     * @requires x.length-offset>=4
     * @effects returns the value of x[offset..offset+4] as an int, 
     *   assuming x is interpreted as a signed big endian number (i.e., 
     *   x[offset] is MSB) If you want to interpret it as an unsigned number,
     *   call ubytes2long on the result.
     */
    public static int beb2int(byte[] x, int offset) {
        //Must mask value after left-shifting, since case from byte
        //to int copies most significant bit to the left!
        int x0=x[offset+3] & 0x000000FF;
        int x1=(x[offset+2]<<8) & 0x0000FF00;
        int x2=(x[offset+1]<<16) & 0x00FF0000;
        int x3=(x[offset]<<24);
        return x3|x2|x1|x0;
    }     
    
    /** 
     * Little-endian bytes to int - stream version
     * 
     */
    public static int leb2int(InputStream is) throws IOException{ 
        //Must mask value after left-shifting, since case from byte
        //to int copies most significant bit to the left!
        int x0=is.read() & 0x000000FF;
        int x1=(is.read()<<8) & 0x0000FF00;
        int x2=(is.read()<<16) & 0x00FF0000;
        int x3=(is.read()<<24);
        return x3|x2|x1|x0;
    }
  
    /** 
     * Little-endian bytes to long.  Unlike leb2int(x, offset), this version can
     * read fewer than 4 bytes.  If n<4, the returned value is never negative.
     * 
     * @param x the source of the bytes
     * @param offset the index to start reading bytes
     * @param n the number of bytes to read, which must be between 1 and 4, 
     *  inclusive
     * @return the value of x[offset..offset+N] as an int, assuming x is 
     *  interpreted as an unsigned little-endian number (i.e., x[offset] is LSB). 
     * @exception IllegalArgumentException n is less than 1 or greater than 4
     * @exception IndexOutOfBoundsException offset<0 or offset+n>x.length
     */
    public static long leb2long(byte[] x, int offset, int n) 
            throws IndexOutOfBoundsException, IllegalArgumentException {
        if (n<1 || n>8)
            throw new IllegalArgumentException("No bytes specified");

        //Must mask value after left-shifting, since case from byte
        //to int copies most significant bit to the left!
        long x0=x[offset] & 0x00000000000000FFL;
        long x1=0;
        long x2=0;
        long x3=0;
        long x4=0;
        long x5=0;
        long x6=0;
        long x7=0;
        if (n>1){
            x1=(x[offset+1]<<8) & 0x000000000000FF00L;
            if (n>2){
                x2=(x[offset+2]<<16) & 0x0000000000FF0000L;
                if (n>3){
                    x3=(x[offset+3]<<24) & 0x00000000FF000000L;
                    if (n>4){
                        x4=(x[offset+4]<<32) & 0x000000FF00000000L;
                        if (n>5){
                            x5=(x[offset+5]<<40) & 0x0000FF0000000000L;
                            if (n>6){
                                x6=(x[offset+6]<<48) & 0x00FF000000000000L;              
                                if (n>7){
                                	x7=(x[offset+7]<<56);
                                }
                            }
                        }
                    }
                }
            }
        }           
        return x7|x6|x5|x4|x3|x2|x1|x0;
    }

    /** 
     * Little-endian bytes to int.  Unlike leb2int(x, offset), this version can
     * read fewer than 4 bytes.  If n<4, the returned value is never negative.
     * 
     * @param x the source of the bytes
     * @param offset the index to start reading bytes
     * @param n the number of bytes to read, which must be between 1 and 4, 
     *  inclusive
     * @return the value of x[offset..offset+N] as an int, assuming x is 
     *  interpreted as an unsigned little-endian number (i.e., x[offset] is LSB). 
     * @exception IllegalArgumentException n is less than 1 or greater than 4
     * @exception IndexOutOfBoundsException offset<0 or offset+n>x.length
     */
    public static int leb2int(byte[] x, int offset, int n) 
            throws IndexOutOfBoundsException, IllegalArgumentException {
        if (n<1 || n>4)
            throw new IllegalArgumentException("No bytes specified");

        //Must mask value after left-shifting, since case from byte
        //to int copies most significant bit to the left!
        int x0=x[offset] & 0x000000FF;
        int x1=0;
        int x2=0;
        int x3=0;
        if (n>1) {
            x1=(x[offset+1]<<8) & 0x0000FF00;
            if (n>2) {
                x2=(x[offset+2]<<16) & 0x00FF0000;
                if (n>3)
                    x3=(x[offset+3]<<24);               
            }
        }
        return x3|x2|x1|x0;
    }

    /**
     * Int to little-endian bytes: writes x to buf[offset..]
     */
    public static void int2leb(int x, byte[] buf, int offset) {
        buf[offset]=(byte)(x & 0x000000FF);
        buf[offset+1]=(byte)((x>>8) & 0x000000FF);
        buf[offset+2]=(byte)((x>>16) & 0x000000FF);
        buf[offset+3]=(byte)((x>>24) & 0x000000FF);
    }

    /**
     * Int to little-endian bytes: writes x to given stream
     */
    public static void int2leb(int x, OutputStream os) throws IOException {
        os.write((byte)(x & 0x000000FF));
        os.write((byte)((x>>8) & 0x000000FF));
        os.write((byte)((x>>16) & 0x000000FF));
        os.write((byte)((x>>24) & 0x000000FF));
    }

    
    /** 
     * Integer/Long to Bytes
     * Specify number of bytes in input variable to convert to bytes
     * 
     * @param dataIN Char, Integer or Long variable
     * @param packet byte array as output packet
     * @param hdrPacket header of the packet being written, or offset.
     * @param bytes number of bytes in input variable Integer(2), Long(4)
     */
	public static void writeBytes(long dataIn, byte packet[], int hdrPacket, int bytes){	
		for(char i=0; i<bytes; i++){
			packet[hdrPacket++] = (byte)(dataIn>>(8*i));
        }		
	}
    
    
   /**
     * Interprets the value of x as an unsigned byte, and returns 
     * it as integer.  For example, ubyte2int(0xFF)==255, not -1.
     */
    public static int ubyte2int(byte x) {
        return ((int)x) & 0x000000FF;
    }

    
}