package com.group8.eece411.KVStore;

public enum KVQueries{

	GET("GET"),
	SET("SET"),
	REMOVE("REMOVE"),
	KILL("KILL"),
	EXIT("EXIT"),
	TEST("TEST"),
	DELNODE("DELNODE");
	

	private final String message;

	private KVQueries(String message){
		this.message = message;
	}

	public String getMessage() {
		return message;		
	}
}