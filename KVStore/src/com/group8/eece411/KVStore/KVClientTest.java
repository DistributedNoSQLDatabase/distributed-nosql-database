package com.group8.eece411.KVStore;

import java.io.*;
import java.net.*;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import static java.util.Arrays.*;

/* UDP Test Client for KVStore
 * Program creates data packet for UDP transmission using unique key and checks reply
 * */

public class KVClientTest {

	public static final boolean DEV_MODE = true;

	public static void main(String args[]) throws Exception {		

		/* Define Options
		 * */
		OptionParser parser = new OptionParser();
		parser.acceptsAll( asList( "h", "?" ), "Show help" ).forHelp();
		OptionSpec<String> set = parser.accepts( "s", "Add value in Key" ).withRequiredArg().ofType(String.class).describedAs("Value");
		OptionSpec<String> setFile = parser.accepts( "S", "Add file Value in Key" ).withRequiredArg().ofType(String.class).describedAs("Value File");
		OptionSpec<String> stressTest = parser.accepts( "T", "Add file Values in Keys" ).withRequiredArg().ofType(String.class).describedAs("Key-Value Pairs File");
		parser.accepts( "g", "Get value from Key" );
		parser.accepts( "r", "Remove value from Key" );
		parser.accepts( "k", "Kill node with Key" );
		OptionSpec<String> hostname = parser.accepts( "n", "Name of single node" ).withRequiredArg().ofType(String.class).describedAs("Hostname").defaultsTo("127.0.0.0");
		OptionSpec<Short> port = parser.accepts( "p", "Port of service" ).withRequiredArg().ofType(Short.class).describedAs("Port").defaultsTo((short)5628);
		OptionSpec<Integer> timeout = parser.accepts( "t", "Time in miliseconds for resend" ).withRequiredArg().ofType(Integer.class).describedAs("Timeout (ms)").defaultsTo(100);
		OptionSpec<String> reqKey = parser.nonOptions("Required Key Value for any action. Default action is Get.").ofType(String.class).describedAs("Value");
		OptionSet options = parser.parse(args);
		
		/* Verify Inputs
		 * */
		if (options.has("h")){
			parser.printHelpOn (System.out);
			System.exit(0);
		}
			
		if(options.nonOptionArguments().size()!=1){
			//Only option without key is Stress Test
			if(!options.has(stressTest)){
				parser.printHelpOn (System.out);
				System.exit(0);				
			}
		}
		
		/* Parse Options
		 * */
		
		InetAddress serverIP = InetAddress.getByName(options.valueOf(hostname));
		short serverPort = options.valueOf(port);
		int timeOutms = options.valueOf(timeout);	
		
		/* Protocol Layer creates packet and fills with UniqueUD
		 * */
		
		KVClient kvStore = new KVClient(serverIP, serverPort,timeOutms);
		
		KVErrors errKV = null;
		
		/* KVStore usage examples
		 * */
		if (options.has(set)){
			errKV = kvStore.set(options.valueOf(reqKey),options.valueOf(set).trim());
			System.out.println(errKV.getMessage());
		}
		else if (options.has(setFile)){
			File valFile = new File(options.valueOf(setFile));
			BufferedReader reader = null;
			String value = null;
			try {
			    reader = new BufferedReader(new FileReader(valFile));

			    if((value = reader.readLine()) == null) {
			        System.err.println("File is empty");
			    }
			} catch (IOException e) {
			    e.printStackTrace();
			} finally {
			    try {
			        if (reader != null) {
			            reader.close();
			        }
			    } catch (IOException e) {
			    }
			}
			errKV = kvStore.set(options.valueOf(reqKey),value.trim());
			System.out.println(errKV.getMessage());
		}
		else if (options.has(stressTest)){
			File valFile = new File(options.valueOf(stressTest));
			BufferedReader reader = null;
			String value = null;
			try {
			    reader = new BufferedReader(new FileReader(valFile));

			    while((value = reader.readLine()) != null) {
			    	String[] KVPair = value.split("\\|");
			    	
			    	long startTime = System.nanoTime();
			    	
			    	errKV = kvStore.set(KVPair[0].trim(),KVPair[1].trim());
			    	
			    	long time = System.nanoTime()-startTime;
			    	
			    	if (errKV.isOK()){
			    		FileWriter writer = new FileWriter("./logTime.log",true);
				    	writer.write(String.valueOf(time/1000) + "\n");
				    	writer.close();	
			    	}else{
			    		FileWriter writer = new FileWriter("./logTime.log",true);
				    	writer.write("Failed " + errKV.getId() + "\n");
			    		System.out.println(errKV.getMessage());
			    		writer.close();	
			    	}

			    }
			} catch (IOException e) {
			    e.printStackTrace();
			} finally {
			    try {
			        if (reader != null) {
			            reader.close();
			        }
			    } catch (IOException e) {
			    }
			}
			
			System.out.println(errKV.getMessage());
			
		}

		else if (options.has("r")){
			errKV = kvStore.remove(options.valueOf(reqKey));
			System.out.println(errKV.getMessage());
		}
		else if (options.has("k")){
			errKV = kvStore.kill();
			System.out.println(errKV.getMessage());
		}
		else { //(options.has("g")) is default action
			System.out.println("VALUE:" + kvStore.get(options.valueOf(reqKey)));
		}			
		

		
		System.exit(0);
	}	
}