package com.group8.eece411.KVStore;

import static java.util.Arrays.asList;

import java.net.InetAddress;
import java.util.Scanner;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

public class KVManager {

	public static void main(String args[]) throws Exception {

		/* Define Options
		 * */
		OptionParser parser = new OptionParser();
		parser.acceptsAll( asList( "h", "?" ), "Show help" ).forHelp();
		OptionSpec<String> hostname = parser.accepts( "n", "Name of single node" ).withRequiredArg().ofType(String.class).describedAs("Hostname").defaultsTo("127.0.0.0");
		OptionSpec<Short> port = parser.accepts( "p", "Port of service" ).withRequiredArg().ofType(Short.class).describedAs("Port").defaultsTo((short)5628);
		OptionSpec<Integer> timeout = parser.accepts( "t", "Time in miliseconds for resend" ).withRequiredArg().ofType(Integer.class).describedAs("Timeout (ms)").defaultsTo(100);
		OptionSet options = parser.parse(args);
		
		/* Verify Inputs
		 * */
		if (options.has("h")){
			parser.printHelpOn (System.out);
			System.exit(0);
		}
			
		/* Parse Options
		 * */
		
		InetAddress serverIP = InetAddress.getByName(options.valueOf(hostname));
		short serverPort = options.valueOf(port);
		int timeOutms = options.valueOf(timeout);
		
		
		KVQueries queryKV = null;
		Scanner cmdPrompt = new Scanner(System.in);
		String cmdLine = null;
		String reqKey = null;
		String reqVal = null;
		
		
		/* Protocol Layer creates packet and fills with UniqueUD
		 * */
		
		KVClient kvStore = new KVClient(serverIP, serverPort,timeOutms);		
		
		/* User Input Loop
		 * */
		
		while(true){

			System.out.print("KVStoreServer> ");
			cmdLine = cmdPrompt.nextLine();

			String[] tokens = cmdLine.split(" ",2);

			if (tokens.length > 1){
				try{
					queryKV = KVQueries.valueOf(tokens[0]);
				}catch (IllegalArgumentException e){
					System.err.println("Query unrecognized");
					continue;
				}
				reqKey = tokens[1];			
			}
			else{
				System.err.println("Request needs at least two tokens\n");
				continue;
			}			

			switch(queryKV){

			case SET:	
				
				tokens = reqKey.split("\\|");
				
				reqKey = tokens[0];
				reqVal = tokens[1];
				kvStore.set(reqKey,reqVal);
				break;

			case GET:
				System.out.println("VALUE:" + kvStore.get(reqKey));
				break;
				
			case REMOVE:
				kvStore.remove(reqKey);
				break;
				
			case KILL:
				kvStore.kill();
				break;
			

			case EXIT: 
				cmdPrompt.close();
				System.exit(0);
				break;
			
			case TEST:
				while(true){
					System.out.println("VALUE:" + kvStore.get(reqKey));
					Thread.sleep(100);
				}
								
			
			default:	
				break;

			}
			System.out.print("\n");
		}



	}  



}
