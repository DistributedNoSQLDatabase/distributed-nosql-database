package com.group8.eece411.KVStore;

import java.util.concurrent.ConcurrentLinkedQueue;

public class LimitedSizeQueue<K> extends ConcurrentLinkedQueue<K> {
	private static final long serialVersionUID = 6083470372345727157L;
	private int maxSize;

    public LimitedSizeQueue(int size){
        this.maxSize = size;
    }

    public boolean add(K k){
        boolean r = super.add(k);
        while (size() > maxSize){
            this.remove();
        }
        return r;
    }
 }
