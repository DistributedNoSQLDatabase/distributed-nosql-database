#!/bin/bash


KV_JAR_PATH=./KVStore-0.0.2.jar

KV_PACKAGE_CLIENT=com.group8.eece411.KVStore.KVClientTest

NODES_LIST=./assi3Nodes.txt

PORT=5600

node1=planetlab2.cs.purdue.edu
node2=planetlab2.cs.ubc.ca
node3=cs-planetlab3.cs.surrey.sfu.ca
node4=planetlab4.cs.uoregon.edu
node5=planetlab1.unr.edu
node6=planetlab2.unr.edu
node7=planetlab1.cs.ucla.edu
node8=planetlab1.eecs.wsu.edu
node9=planetlab1.unl.edu
node0=planetlab-1.cs.uic.edu

case $@ in

SET15k)
	#for line in $(cat $NODES_LIST)		
	for node in $node4 #$node2 $node3 $node4 $node5 $node6 $node7 $node8 $node9 $node0;
		do
			#SET from file
			command="java -cp $KV_JAR_PATH $KV_PACKAGE_CLIENT -n $line -p $PORT -S ./testVal.txt mykey"

			echo $command
			time $command

			sleep 5
			command="java -cp $KV_JAR_PATH $KV_PACKAGE_CLIENT -n $line -p $PORT -t 1000 -g mykey"
			echo $command
			time $command

		done
	;;
KILL)	
	command="java -cp $KV_JAR_PATH $KV_PACKAGE_CLIENT -n $node1 -p $PORT -k mykey"
	echo $command
	time $command
	;;
REMOVE)
	command="java -cp $KV_JAR_PATH $KV_PACKAGE_CLIENT -n $node -p $PORT -r 2"
	echo $command
	time $command
	;;

SET)
	command="java -cp $KV_JAR_PATH $KV_PACKAGE_CLIENT -n $node -p $PORT -s 85 mykey"
	echo $command
	time $command
	;;

TEST)
	command="java -cp $KV_JAR_PATH $KV_PACKAGE_CLIENT -n $node1 -p $PORT -T ./KVData.csv" 
	echo $command
	$command

	#command="java -cp $KV_JAR_PATH $KV_PACKAGE_CLIENT -n $node1 -p $PORT -T ./KVLargeData.csv" 
	#echo $command
	#$command
	;;

*) echo this is *
	;;
esac



