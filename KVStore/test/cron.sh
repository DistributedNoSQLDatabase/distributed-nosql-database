#!/bin/bash

#TEST_FILE=./assi4Nodes/*
TEST_FILE=./assi4Nodes/assi4Nodes_3.txt

CRON_FILE=./group8/cronfile


for try in 1
do
	for file in $TEST_FILE
	do
		echo $file

		parallel-ssh -h $file -l ubc_eece411_4 -t 120 -p 200 "crontab -l | tee $CRON_FILE"

		parallel-ssh -h $file -l ubc_eece411_4 -t 120 -p 200 "echo 0 0 0 0 0 some entry >> $CRON_FILE"

		parallel-ssh -h $file -l ubc_eece411_4 -t 120 -p 200 "cat $CRON_FILE|crontab -"
		
		

	done

done
