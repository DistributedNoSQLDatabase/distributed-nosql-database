#!/bin/bash

KV_JAR_PATH=./KVStore-0.0.2.jar

#Update from Maven Build
cp ../target/KVStore-0.0.2.jar $KV_JAR_PATH


TEST_FILE=./serviceNodes.txt
KV_PACKAGE_CLIENT=com.group8.eece411.KVStore.KVClientTest



node1=planetlab4.cs.uoregon.edu
node2=planetlab2.cs.ubc.ca
node3=cs-planetlab3.cs.surrey.sfu.ca
node4=planetlab2.cs.purdue.edu


scp $KV_JAR_PATH  ubc_eece411_4@$node1:./group8/
scp $TEST_FILE  ubc_eece411_4@$node1:./group8/
scp $KV_JAR_PATH  ubc_eece411_4@$node2:./group8/
scp $TEST_FILE  ubc_eece411_4@$node2:./group8/
scp $KV_JAR_PATH  ubc_eece411_4@$node3:./group8/
scp $TEST_FILE  ubc_eece411_4@$node3:./group8/
scp $KV_JAR_PATH  ubc_eece411_4@$node4:./group8/
scp $TEST_FILE  ubc_eece411_4@$node4:./group8/


