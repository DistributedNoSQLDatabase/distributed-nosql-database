SET


java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab2.cs.purdue.edu -p 5600 -s 45 2
0.12user 0.02system 0:00.18elapsed 81%CPU (0avgtext+0avgdata 94368maxresident)k
0inputs+64outputs (0major+8166minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab2.cs.ubc.ca -p 5600 -s 45 2
0.13user 0.02system 0:00.13elapsed 120%CPU (0avgtext+0avgdata 95376maxresident)k
0inputs+64outputs (0major+8251minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n cs-planetlab3.cs.surrey.sfu.ca -p 5600 -s 45 2
0.12user 0.04system 0:00.12elapsed 124%CPU (0avgtext+0avgdata 95376maxresident)k
0inputs+64outputs (0major+8252minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab4.cs.uoregon.edu -p 5600 -s 45 2
0.13user 0.02system 0:00.14elapsed 114%CPU (0avgtext+0avgdata 95344maxresident)k
0inputs+64outputs (0major+8257minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab1.unr.edu -p 5600 -s 45 2
0.12user 0.03system 0:00.15elapsed 102%CPU (0avgtext+0avgdata 94256maxresident)k
0inputs+64outputs (0major+8162minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab2.unr.edu -p 5600 -s 45 2
0.14user 0.02system 0:00.15elapsed 103%CPU (0avgtext+0avgdata 94320maxresident)k
0inputs+64outputs (0major+8165minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab1.cs.ucla.edu -p 5600 -s 45 2
0.14user 0.02system 0:00.15elapsed 101%CPU (0avgtext+0avgdata 94176maxresident)k
0inputs+64outputs (0major+8169minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab1.unl.edu -p 5600 -s 45 2
0.12user 0.03system 0:00.17elapsed 86%CPU (0avgtext+0avgdata 94400maxresident)k
0inputs+64outputs (0major+8167minor)pagefaults 0swaps


GET


java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab2.cs.purdue.edu -p 5600 -g 2
Length: 2
VALUE:45
0.16user 0.02system 0:00.20elapsed 86%CPU (0avgtext+0avgdata 94960maxresident)k
0inputs+64outputs (0major+8547minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab2.cs.ubc.ca -p 5600 -g 2
Length: 2
VALUE:45
0.14user 0.02system 0:00.14elapsed 117%CPU (0avgtext+0avgdata 95552maxresident)k
0inputs+64outputs (0major+8465minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n cs-planetlab3.cs.surrey.sfu.ca -p 5600 -g 2
Length: 2
VALUE:45
0.13user 0.02system 0:00.14elapsed 113%CPU (0avgtext+0avgdata 94336maxresident)k
0inputs+64outputs (0major+8467minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab4.cs.uoregon.edu -p 5600 -g 2
Length: 2
VALUE:45
0.14user 0.02system 0:00.15elapsed 111%CPU (0avgtext+0avgdata 93696maxresident)k
0inputs+64outputs (0major+8464minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab1.unr.edu -p 5600 -g 2
Length: 2
VALUE:45
0.13user 0.04system 0:00.17elapsed 102%CPU (0avgtext+0avgdata 95152maxresident)k
0inputs+64outputs (0major+8486minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab2.unr.edu -p 5600 -g 2
Length: 2
VALUE:45
0.15user 0.01system 0:00.16elapsed 102%CPU (0avgtext+0avgdata 94096maxresident)k
0inputs+64outputs (0major+8478minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab1.cs.ucla.edu -p 5600 -g 2
Length: 2
VALUE:45
0.14user 0.03system 0:00.17elapsed 97%CPU (0avgtext+0avgdata 94304maxresident)k
0inputs+64outputs (0major+8465minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab1.unl.edu -p 5600 -g 2
Length: 2
VALUE:45
0.15user 0.02system 0:00.19elapsed 92%CPU (0avgtext+0avgdata 95392maxresident)k
0inputs+64outputs (0major+8484minor)pagefaults 0swaps



REMOVE


java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab2.cs.purdue.edu -p 5600 -r 2
0.13user 0.02system 0:00.18elapsed 81%CPU (0avgtext+0avgdata 94096maxresident)k
0inputs+64outputs (0major+8167minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab2.cs.ubc.ca -p 5600 -r 2
0.13user 0.02system 0:00.13elapsed 122%CPU (0avgtext+0avgdata 94288maxresident)k
0inputs+64outputs (0major+8163minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n cs-planetlab3.cs.surrey.sfu.ca -p 5600 -r 2
0.14user 0.01system 0:00.12elapsed 125%CPU (0avgtext+0avgdata 95408maxresident)k
0inputs+64outputs (0major+8253minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab4.cs.uoregon.edu -p 5600 -r 2
0.12user 0.02system 0:00.13elapsed 110%CPU (0avgtext+0avgdata 94208maxresident)k
0inputs+64outputs (0major+8167minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab1.unr.edu -p 5600 -r 2
0.13user 0.02system 0:00.15elapsed 101%CPU (0avgtext+0avgdata 94256maxresident)k
0inputs+64outputs (0major+8163minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab2.unr.edu -p 5600 -r 2
0.13user 0.01system 0:00.15elapsed 100%CPU (0avgtext+0avgdata 94224maxresident)k
0inputs+64outputs (0major+8165minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab1.cs.ucla.edu -p 5600 -r 2
0.13user 0.02system 0:00.15elapsed 102%CPU (0avgtext+0avgdata 95232maxresident)k
0inputs+64outputs (0major+8258minor)pagefaults 0swaps
java -cp ./KVStore-0.0.1-SNAPSHOT.jar com.group8.eece411.KVStore.KVClientTest -n planetlab1.unl.edu -p 5600 -r 2
0.13user 0.02system 0:00.17elapsed 93%CPU (0avgtext+0avgdata 95152maxresident)k
0inputs+64outputs (0major+8257minor)pagefaults 0swaps

