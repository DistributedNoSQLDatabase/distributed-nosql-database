#!/bin/bash

KV_JAR_PATH=./KVStore-0.0.2.jar

#Update from Maven Build
cp ../target/KVStore-0.0.2.jar $KV_JAR_PATH


#TEST_FILE=./assi4Nodes/*
TEST_FILE=./assi4Nodes/assi4Nodes_1.txt

for file in $TEST_FILE
do
	echo $file

	parallel-ssh -h $file -l ubc_eece411_4 -t 120 -p 200 "java -cp /home/ubc_eece411_4/group8/KVStore-0.0.2.jar com.group8.eece411.KVStore.KVClientTest -k -p 5630 key"
	#parallel-ssh -h $file -l ubc_eece411_4 -t 120 -p 200 "java -cp /home/ubc_eece411_4/group8/KVStore-0.0.2.jar com.group8.eece411.KVStore.KVClientTest -k -p 5600 key"
	#parallel-ssh -h $file -l ubc_eece411_4 -t 120 -p 200 "java -cp /home/ubc_eece411_4/group8/KVStore-0.0.2.jar com.group8.eece411.KVStore.KVClientTest -k -p 5601 key"

done



